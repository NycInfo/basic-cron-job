﻿var winston = require('winston');
winston.transports.DailyRotateFile = require('winston-daily-rotate-file');
var mkpath = require('mkpath');
const { format } = require('winston');
const { combine, timestamp, json } = format;

var dirctoryPath = 'logs/';
mkpath(dirctoryPath, function (err) {
    if (err)
        throw err;
});

mkpath.sync(dirctoryPath, 0700);

var logger = winston.createLogger({
    format: combine(
        timestamp(),
        json()
    ),
    transports: [
        new (winston.transports.DailyRotateFile)({
            name: 'file',
            filename: 'logs/STX-Crons-Logs-%DATE%.log',
            datePattern: 'YYYY-MM-DD',
            level: 'debug',
            handleExceptions: true,
            humanReadableUnhandledException: true,
        })
    ],
    exitOnError: false
});

logger.setMaxListeners(0);

module.exports = logger;
